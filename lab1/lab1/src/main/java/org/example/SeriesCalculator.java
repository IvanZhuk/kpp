package org.example;

public class SeriesCalculator {
    public Fraction calculate(int n) {
        if (n <= 15) {
            return calculateInt(n);
        }
        else {
            return calculateBigInt(n);
        }
    }

    private FractionInt calculateInt(int n) {
        FractionInt sum = new FractionInt(0,1);

        for (int i = 1; i <= n; i++) {
            FractionInt t = new FractionInt(1, i);
            sum = sum.add(t);
        }

        sum.simplify();
        return sum;
    }

    private FractionBigInt calculateBigInt(int n) {
        FractionBigInt sum = new FractionBigInt(0,1);

        for (int i = 1; i <= n; i++) {
            FractionBigInt t = new FractionBigInt(1, i);
            sum = sum.add(t);
        }

        sum.simplify();
        return sum;
    }
}
