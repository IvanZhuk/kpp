package org.example;

public class FractionInt implements Fraction {
    private int nominator;
    private int denominator;

    public FractionInt(int n, int d) {
        nominator = n;
        denominator = d;
    }

    public void setNominator(int n) { nominator = n; }
    public void setDenominator(int d) { denominator = d; }
    public int getNominator() { return nominator; }
    public int getDenominator() { return denominator; }

    public FractionInt add(Fraction other) {
        FractionInt result = new FractionInt(1,1);

        result.setNominator(this.nominator * ((FractionInt)other).getDenominator() +
                this.denominator * ((FractionInt)other).getNominator());
        result.setDenominator(this.denominator * ((FractionInt)other).getDenominator());

        return result;
    }

    public void simplify() {
        int r = gcd(nominator, denominator);
        nominator /= r;
        denominator /= r;
    }

    public int gcd(int a, int b) {
        int t;
        while (b != 0){
            t = b;
            b = a % b;
            a = t;
        }
        return a;
    }

    public void display() {
        System.out.println(nominator + "/" + denominator);
        System.out.println(nominator * 1.0 / denominator);
    }
}
