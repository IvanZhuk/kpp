package org.example;

public interface Fraction {
    Fraction add(Fraction other);

    void simplify();

    void display();
}
