package org.example;

import java.math.BigInteger;

public class FractionBigInt implements Fraction {
    private BigInteger nominator;
    private BigInteger denominator;

    public FractionBigInt(int n, int d) {
        nominator = BigInteger.valueOf(n);
        denominator = BigInteger.valueOf(d);
    }

    public void setNominator(BigInteger n) { nominator = n; }
    public void setDenominator(BigInteger d) { denominator = d; }
    public BigInteger getNominator() { return nominator; }
    public BigInteger getDenominator() { return denominator; }

    public FractionBigInt add(Fraction other) {
        FractionBigInt result = new FractionBigInt(1,1);

        result.setNominator(this.nominator.multiply(((FractionBigInt)other).getDenominator()).
                add(this.denominator.multiply(((FractionBigInt)other).getNominator())));
        result.setDenominator(this.denominator.multiply(((FractionBigInt)other).getDenominator()));

        return result;
    }

    public void simplify() {
        BigInteger r = gcd(nominator, denominator);
        nominator = nominator.divide(r);
        denominator = denominator.divide(r);
    }

    public BigInteger gcd(BigInteger a, BigInteger b) {
        BigInteger t;
        while (!b.equals(BigInteger.ZERO)){
            t = b;
            b = a.mod(b);
            a = t;
        }
        return a;
    }

    public void display() {
        System.out.println(nominator + "/" + denominator);
        System.out.println(nominator.doubleValue() / denominator.doubleValue());
    }
}
