package org.example;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Input number of elements to sum: ");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        SeriesCalculator scl = new SeriesCalculator();
        Fraction res = scl.calculate(n);

        System.out.println("\nResult:");
        res.display();
    }
}